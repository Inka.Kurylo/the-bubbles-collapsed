
let button = document.querySelector('button');
button.style.background='red';
button.style.border = 'none';
button.style.height = '40px';
button.style.color = 'white';
button.style.fontSize = '22px'
button.style.borderRadius = '4px';
let divSector = document.createElement('sector');
divSector.style.width= '100%';
divSector.style.display='flex';
divSector.style.flexWrap='wrap';
document.body.append(divSector);
window.addEventListener('click',(event)=>{
    if (event.target.closest('button')){
        let diametr = prompt('input diametr:');
        for (let i=1; i<100; i++){
            let div = document.createElement('div');
            div.style.height = diametr+'px';
            div.style.width = diametr+'px';
            randColor(div);
            div.style.borderRadius='50%';   
            divSector.append(div);
        }
    }
    if (event.target.closest('div')){
        event.target.style.display='none';
    }
 
})
function randColor(elem) {
                    r = Math.floor(Math.random() * (255)),
                    g = Math.floor(Math.random() * (255)),
                    b = Math.floor(Math.random() * (255)),
                    color = `rgb(${r},${g},${b})`;
                    elem.style.background = color;
            }